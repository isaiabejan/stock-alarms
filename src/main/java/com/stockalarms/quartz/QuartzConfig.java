package com.stockalarms.quartz;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {

    @Bean("alarmJobDetail")
    public JobDetail alarmJobDetail() {
        return JobBuilder.newJob(AlarmCheckJob.class)
                .withIdentity("alarmJob")
                .storeDurably()
                .build();
    }

    @Bean("alarmCheckTrigger")
    public Trigger alarmCheckTrigger(
            @Qualifier("alarmJobDetail") JobDetail alarmCheckJob,
            @Value("${check.schedule}") String checkSchedule) {
        return TriggerBuilder.newTrigger()
                .withIdentity("alarmCheckTrigger")
                .forJob(alarmCheckJob)
                .withSchedule(CronScheduleBuilder.cronSchedule(checkSchedule))
                .build();
    }

}