package com.stockalarms.quartz;

import com.stockalarms.core.service.AlarmService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DisallowConcurrentExecution
public class AlarmCheckJob implements Job {

    private static final Logger LOG = LoggerFactory.getLogger(AlarmCheckJob.class);

    private final AlarmService alarmService;

    public AlarmCheckJob(AlarmService alarmService) {
        this.alarmService = alarmService;
    }

    @Override
    public void execute(JobExecutionContext context) {

        LOG.info("Check alarms job starting...");

        alarmService.checkAlarms();

        LOG.info("Check alarms job done.");
    }

}
