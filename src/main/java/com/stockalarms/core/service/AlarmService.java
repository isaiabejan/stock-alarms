package com.stockalarms.core.service;

import com.stockalarms.core.converter.AlarmConverter;
import com.stockalarms.core.converter.StockConverter;
import com.stockalarms.core.dto.Alarm;
import com.stockalarms.core.dto.Stock;
import com.stockalarms.core.persistence.entity.AlarmEntity;
import com.stockalarms.core.persistence.entity.UserEntity;
import com.stockalarms.core.persistence.repository.AlarmRepository;
import com.stockalarms.mail.MailDetails;
import com.stockalarms.mail.MailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.stockalarms.core.converter.UserConverter.convert;
import static java.util.stream.Collectors.toList;

@Service
public class AlarmService {

    private final MailService mailService;
    private final UserService userService;
    private final StockService stockService;
    private final AlarmRepository alarmRepository;

    public AlarmService(MailService mailService,
                        UserService userService,
                        StockService stockService,
                        AlarmRepository alarmRepository) {
        this.mailService = mailService;
        this.userService = userService;
        this.stockService = stockService;
        this.alarmRepository = alarmRepository;
    }

    public Alarm save(Long stockId, Integer percentage, Integer sign) {

        Stock stock = stockService.getById(stockId)
                .orElseThrow(() -> new RuntimeException("No stock found with id = " + stockId));

        AlarmEntity alarmEntity = new AlarmEntity();
        alarmEntity.setActive(true);
        alarmEntity.setStock(StockConverter.convert(stock));
        alarmEntity.computePrice(percentage, sign);
        alarmEntity.setUser(convert(userService.getCurrentUser()));

        AlarmEntity savedAlarm = alarmRepository.save(alarmEntity);

        return AlarmConverter.convert(savedAlarm);
    }

    public List<Alarm> getAll() {
        UserEntity currentUser = convert(userService.getCurrentUser());
        return alarmRepository.findAllByUser(currentUser)
                .stream()
                .map(AlarmConverter::convert)
                .collect(toList());
    }

    public void delete(Long id) {
        alarmRepository.deleteById(id);
    }

    @Transactional
    public void checkAlarms() {
        List<Stock> stocks = stockService.getAll();

        stocks.forEach(stock -> {
            Double stockPrice = stockService.getStockPrice(stock.getSymbol());
            stockService.updateStockPrice(stockPrice, stock.getId());
            List<AlarmEntity> alarms = alarmRepository.findAllByStock(StockConverter.convert(stock));
            alarms.forEach(alarm -> {
                if (stockPrice >= alarm.getPrice() && alarm.isActive()) {
                    alarmRepository.save(alarm.active(false));
                    mailService.sendMail(new MailDetails()
                            .to(alarm.getUser().getEmail())
                            .subject(stock.getSymbol())
                            .message("Alarm triggered"));
                }
            });
        });
    }

}
