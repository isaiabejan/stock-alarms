package com.stockalarms.core.service;

import com.stockalarms.alphavantage.model.GlobalQuote;
import com.stockalarms.alphavantage.model.Response;
import com.stockalarms.alphavantage.service.AlphaVantageService;
import com.stockalarms.core.converter.AlarmConverter;
import com.stockalarms.core.converter.StockConverter;
import com.stockalarms.core.converter.UserConverter;
import com.stockalarms.core.dto.Stock;
import com.stockalarms.core.persistence.entity.AlarmEntity;
import com.stockalarms.core.persistence.entity.StockEntity;
import com.stockalarms.core.persistence.entity.UserEntity;
import com.stockalarms.core.persistence.repository.AlarmRepository;
import com.stockalarms.core.persistence.repository.StockRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.stockalarms.core.converter.StockConverter.convert;
import static com.stockalarms.core.converter.StockConverter.convertToEntity;
import static java.util.stream.Collectors.toList;

@Service
public class StockService {

    private final UserService userService;
    private final StockRepository stockRepository;
    private final AlarmRepository alarmRepository;
    private final AlphaVantageService alphaVantageService;

    public StockService(UserService userService,
                        StockRepository stockRepository,
                        AlarmRepository alarmRepository,
                        AlphaVantageService alphaVantageService) {
        this.userService = userService;
        this.stockRepository = stockRepository;
        this.alarmRepository = alarmRepository;
        this.alphaVantageService = alphaVantageService;
    }

    public Stock add(String symbol) {
        Response response = alphaVantageService.get(symbol);
        if (response == null) {
            throw new RuntimeException("No stock found with symbol = " + symbol);
        }
        StockEntity stock = convertToEntity(response.getGlobalQuote());

        Optional<StockEntity> optionalStock = stockRepository.findBySymbol(symbol);

        StockEntity stockEntity;
        UserEntity currentUser = UserConverter.convert(userService.getCurrentUser());
        if (optionalStock.isPresent()) {
            StockEntity savedStock = optionalStock.get();
            savedStock.addUser(currentUser);
            savedStock.setPrice(stock.getPrice());
            stockEntity = stockRepository.save(savedStock);
        } else {
            stock.addUser(currentUser);
            stockEntity = stockRepository.save(stock);
        }

        return convert(stockEntity);
    }

    public List<Stock> getAll() {
        return stockRepository.findAll()
                .stream()
                .map(StockConverter::convert)
                .collect(toList());
    }

    public List<Stock> getAllForCurrentUser() {
        UserEntity currentUser = UserConverter.convert(userService.getCurrentUser());

        return stockRepository.findAllByUsersContains(currentUser)
                .stream()
                .map(stockEntity -> {
                    Stock stock = convert(stockEntity);
                    List<AlarmEntity> alarmEntities = alarmRepository.findAllByUserAndStock(currentUser, stockEntity);
                    stock.setAlarms(AlarmConverter.convertEntities(alarmEntities));
                    return stock;
                })
                .collect(toList());
    }

    public Double getStockPrice(String symbol) {
        Response response = alphaVantageService.get(symbol);
        if (response == null) {
            throw new RuntimeException("No stock found with symbol = " + symbol);
        }
        GlobalQuote globalQuote = response.getGlobalQuote();

        return Double.parseDouble(globalQuote.getPrice());
    }

    public Optional<Stock> getById(Long id) {
        return stockRepository.findById(id).map(StockConverter::convert);
    }

    public void updateStockPrice(Double price, Long id) {
        stockRepository.setPriceForStock(price, id);
    }

}
