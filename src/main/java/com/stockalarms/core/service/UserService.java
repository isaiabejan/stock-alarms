package com.stockalarms.core.service;

import com.stockalarms.core.converter.UserConverter;
import com.stockalarms.core.dto.User;
import com.stockalarms.core.persistence.entity.UserEntity;
import com.stockalarms.core.persistence.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getCurrentUser() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication auth = securityContext.getAuthentication();

        if (auth == null) {
            return null;
        }

        final Object principal = auth.getPrincipal();
        if (principal instanceof UserDetails) {
            UserEntity userEntity = userRepository.findByEmail(((UserDetails) principal).getUsername())
                    .orElseThrow(() -> new RuntimeException("No user found"));
            return UserConverter.convert(userEntity);
        }

        return null;
    }

}
