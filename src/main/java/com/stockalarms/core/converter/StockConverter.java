package com.stockalarms.core.converter;

import com.stockalarms.alphavantage.model.GlobalQuote;
import com.stockalarms.core.dto.Stock;
import com.stockalarms.core.persistence.entity.StockEntity;

public class StockConverter {

    public static Stock convert(StockEntity stockEntity) {
        if (stockEntity == null)
            return null;

        Stock stock = new Stock();

        stock.setId(stockEntity.getId());
        stock.setSymbol(stockEntity.getSymbol());
        stock.setPrice(stockEntity.getPrice());

        return stock;
    }

    public static StockEntity convert(Stock stock) {
        if (stock == null)
            return null;

        StockEntity stockEntity = new StockEntity();

        stockEntity.setId(stock.getId());
        stockEntity.setSymbol(stock.getSymbol());
        stockEntity.setPrice(stock.getPrice());

        return stockEntity;
    }

    public static StockEntity convertToEntity(GlobalQuote globalQuote) {
        if (globalQuote == null)
            return null;

        StockEntity stockEntity = new StockEntity();

        stockEntity.setSymbol(globalQuote.getSymbol());
        stockEntity.setPrice(Double.parseDouble(globalQuote.getPrice()));

        return stockEntity;
    }
    public static Stock convertToDto(GlobalQuote globalQuote) {
        if (globalQuote == null)
            return null;

        Stock stock = new Stock();

        stock.setSymbol(globalQuote.getSymbol());
        stock.setPrice(Double.parseDouble(globalQuote.getPrice()));

        return stock;
    }

}
