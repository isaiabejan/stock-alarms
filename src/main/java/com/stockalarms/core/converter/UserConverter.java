package com.stockalarms.core.converter;

import com.stockalarms.core.dto.User;
import com.stockalarms.core.persistence.entity.UserEntity;

public class UserConverter {

    public static User convert(UserEntity userEntity) {
        if (userEntity == null)
            return null;

        User user = new User();

        user.setId(userEntity.getId());
        user.setFirstname(userEntity.getFirstname());
        user.setLastname(userEntity.getLastname());
        user.setPassword(userEntity.getPassword());
        user.setEmail(userEntity.getEmail());

        return user;
    }

    public static UserEntity convert(User user) {
        if (user == null)
            return null;

        UserEntity userEntity = new UserEntity();

        userEntity.setId(user.getId());
        userEntity.setFirstname(user.getFirstname());
        userEntity.setLastname(user.getLastname());
        userEntity.setPassword(user.getPassword());
        userEntity.setEmail(user.getEmail());

        return userEntity;
    }

}
