package com.stockalarms.core.converter;

import com.stockalarms.core.dto.Alarm;
import com.stockalarms.core.persistence.entity.AlarmEntity;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class AlarmConverter {

    public static Alarm convert(AlarmEntity alarmEntity) {
        if (alarmEntity == null)
            return null;

        Alarm alarm = new Alarm();
        alarm.setId(alarmEntity.getId());
        alarm.setPrice(alarmEntity.getPrice());
        alarm.setTriggered(alarmEntity.isActive());
        alarm.setUserId(alarmEntity.getUser().getId());

        return alarm;
    }

    public static AlarmEntity convert(Alarm alarm) {
        if (alarm == null)
            return null;

        AlarmEntity alarmEntity = new AlarmEntity();
        alarmEntity.setId(alarm.getId());
        alarmEntity.setPrice(alarm.getPrice());
        alarmEntity.setActive(alarm.isTriggered());

        return alarmEntity;
    }

    public static List<Alarm> convertEntities(List<AlarmEntity> alarmEntities) {
        if (alarmEntities == null)
            return emptyList();

        return alarmEntities
                .stream()
                .map(AlarmConverter::convert)
                .collect(toList());
    }

}
