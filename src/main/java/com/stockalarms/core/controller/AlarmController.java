package com.stockalarms.core.controller;

import com.stockalarms.core.dto.Alarm;
import com.stockalarms.core.service.AlarmService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/alarms")
public class AlarmController {

    private final AlarmService alarmService;

    public AlarmController(AlarmService alarmService) {
        this.alarmService = alarmService;
    }

    @PostMapping
    public Alarm save(Long stockId, Integer percentage, Integer sign) {
        return alarmService.save(stockId, percentage, sign);
    }

    @GetMapping
    public List<Alarm> getAll() {
        return alarmService.getAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        alarmService.delete(id);
    }
}
