package com.stockalarms.core.controller;

import com.stockalarms.core.dto.Stock;
import com.stockalarms.core.service.StockService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stocks")
public class StockController {

    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @PostMapping
    public Stock add(String symbol) {
        return stockService.add(symbol);
    }

    @GetMapping
    public List<Stock> getAll() {
        return stockService.getAllForCurrentUser();
    }

}
