package com.stockalarms.core.controller;

import com.stockalarms.auth.AuthUser;
import com.stockalarms.auth.JwtUserDetailsService;
import com.stockalarms.auth.config.JwtUtil;
import com.stockalarms.core.dto.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    private final JwtUtil jwtUtil;
    private final JwtUserDetailsService jwtUserDetailsService;
    private final AuthenticationManager authenticationManager;

    public AuthController(JwtUtil jwtUtil,
                          JwtUserDetailsService jwtUserDetailsService,
                          AuthenticationManager authenticationManager) {
        this.jwtUtil = jwtUtil;
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/authenticate")
    public String authenticate(@RequestBody AuthUser authUser) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authUser.getEmail(), authUser.getPassword()));

        UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authUser.getEmail());

        return jwtUtil.generateToken(userDetails);
    }

    @PostMapping("/register")
    public void register(@RequestBody User user) {
        jwtUserDetailsService.save(user);
    }

}

