package com.stockalarms.core.persistence.entity;

import javax.persistence.*;

@Entity(name = "alarms")
public class AlarmEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double price;
    private boolean active;
    @ManyToOne
    private StockEntity stock;
    @ManyToOne
    private UserEntity user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean triggered) {
        this.active = triggered;
    }

    public AlarmEntity active(boolean triggered) {
        this.active = triggered;
        return this;
    }

    public StockEntity getStock() {
        return stock;
    }

    public void setStock(StockEntity stockEntity) {
        this.stock = stockEntity;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public void computePrice(Integer percentage, Integer sign) {
        Double stockPrice = stock.getPrice();
        this.price = stockPrice + sign * (percentage * stockPrice) / 100;
    }

}
