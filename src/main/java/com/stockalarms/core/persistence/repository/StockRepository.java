package com.stockalarms.core.persistence.repository;

import com.stockalarms.core.persistence.entity.StockEntity;
import com.stockalarms.core.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StockRepository extends JpaRepository<StockEntity, Long> {

    Optional<StockEntity> findBySymbol(String symbol);

    List<StockEntity> findAllByUsersContains(UserEntity user);

    @Modifying
    @Query("update stocks s set s.price = :price where s.id = :id")
    void setPriceForStock(@Param("price") Double price, @Param("id") Long id);

}
