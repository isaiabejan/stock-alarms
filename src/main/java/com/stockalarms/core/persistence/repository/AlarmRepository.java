package com.stockalarms.core.persistence.repository;

import com.stockalarms.core.persistence.entity.AlarmEntity;
import com.stockalarms.core.persistence.entity.StockEntity;
import com.stockalarms.core.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlarmRepository extends JpaRepository<AlarmEntity, Long> {

    List<AlarmEntity> findAllByUser(UserEntity user);

    List<AlarmEntity> findAllByStock(StockEntity stock);

    List<AlarmEntity> findAllByUserAndStock(UserEntity user, StockEntity stock);

}
