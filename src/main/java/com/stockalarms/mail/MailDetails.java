package com.stockalarms.mail;

public class MailDetails {

    String to;
    String message;
    String subject;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public MailDetails to(String to) {
        this.to = to;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MailDetails message(String message) {
        this.message = message;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public MailDetails subject(String subject) {
        this.subject = subject;
        return this;
    }

}
