package com.stockalarms.alphavantage.service;

import com.stockalarms.alphavantage.model.Response;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AlphaVantageService {

    private static final String API_KEY = "DZQV1Q956U342ISJ";
    private static final String API_BASE_URL = "https://www.alphavantage.co/";

    public Response get(String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        String url = API_BASE_URL + "query?function=GLOBAL_QUOTE&symbol=" + symbol + "&apikey=" + API_KEY;

        return restTemplate.getForObject(url, Response.class);
    }

}
