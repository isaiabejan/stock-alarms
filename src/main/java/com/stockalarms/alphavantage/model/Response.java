package com.stockalarms.alphavantage.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {

    @JsonProperty("Global Quote")
    private GlobalQuote globalQuote;

    public GlobalQuote getGlobalQuote() {
        return globalQuote;
    }

    public void setGlobalQuote(GlobalQuote GlobalQuote) {
        this.globalQuote = GlobalQuote;
    }

}
