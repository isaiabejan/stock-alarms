package com.stockalarms.alphavantage;

import com.stockalarms.alphavantage.service.AlphaVantageService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigTest {

    @Bean
    public AlphaVantageService alphaVantageService() {
        return new AlphaVantageService();
    }

}
