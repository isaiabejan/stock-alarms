package com.stockalarms.alphavantage.service;

import com.stockalarms.alphavantage.ConfigTest;
import com.stockalarms.alphavantage.model.GlobalQuote;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ConfigTest.class)
public class AlphaVantageServiceTest {

    @Autowired
    private AlphaVantageService alphaVantageService;

    @Test
    void getStock() {
        GlobalQuote response = alphaVantageService.get("AAPL").getGlobalQuote();

        assertThat(response.getSymbol()).isEqualTo("AAPL");
    }

}
